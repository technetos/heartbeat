use std::{io, sync::{mpsc, Arc, Mutex, Condvar}, thread};

pub struct UserInputEventSource {
  events: thread::JoinHandle<()>,
}

impl UserInputEventSource {
  pub fn new(client: mpsc::Sender<String>, condition: Arc<(Mutex<bool>, Condvar)>) -> Self {
    let events_thread = thread::spawn(move || {
      loop {
        let mut user_input = String::new();
        // Block thread until the user presses enter
        io::stdin().read_line(&mut user_input);

        // Send the client thread the user input
        client.send(user_input).unwrap();

        // Let the client know it should wake up and process the user input
        let &(ref lock, ref var) = &*condition;
        let mut event_waiting = lock.lock().unwrap();
        *event_waiting = true;
        var.notify_one();
      }
    });
    
    UserInputEventSource {
      events: events_thread,
    }
  }
}
