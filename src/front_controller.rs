pub struct FrontController;

impl FrontController {
  pub fn new() -> Self {
    FrontController {}
  }

  pub fn dispatch(&self, event: String) {
    println!("{}", event);
  }
}
