extern crate heartbeat;

use heartbeat::{
  user_input_event_source::UserInputEventSource,
  front_controller::FrontController,
};

use std::{
  sync::{mpsc, Mutex, Arc, Condvar},
  time::Duration,
  thread,
};

fn main() {
  let (tx, rx) = mpsc::channel();
  let condition = Arc::new((Mutex::new(false), Condvar::new()));

  UserInputEventSource::new(tx, condition.clone());
  let controller = FrontController::new();

  let _ = thread::spawn(move || {
    let &(ref lock, ref var) = &*condition;
    let mut event_waiting = lock.lock().unwrap();
    while !*event_waiting {
      event_waiting = var.wait(event_waiting).unwrap();
      controller.dispatch(rx.recv().unwrap());
      *event_waiting = false;
    }
  }).join();
}
